import { Meteor } from 'meteor/meteor'

const state = {
	data: Meteor.user()
}

const actions = {
	
}

export default {
	namespaced: true,
	state,
	actions
}