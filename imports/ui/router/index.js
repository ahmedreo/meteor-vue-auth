import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '../components/Welcome'
import Dashboard from '../components/Dashboard'
import Login from '../components/auth/Login'
import Register from '../components/auth/Register'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			component: Welcome
		},
		{
			path: '/dashboard',
			component: Dashboard
		},
		{
			path: '/login',
			component: Login
		},
		{
			path: '/register',
			component: Register
		}
	]
})