## Initialize Meteor
- npm install
- meteor npm install --save bcrypt | or https://forums.meteor.com/t/how-to-fix-the-bcrypt-error-in-windows-7/28856/25?u=samahudin
- meteor

## Deploy to Heroku
- heroku create your-app-name
- heroku config:set ROOT_URL=your-heroku-url --app your-app-name
- heroku config:set MONGO_URL=your-mlab-db-uri --app your-app-name
- heroku config:set BUILDPACK_PRELAUNCH_METEOR=1 --app your-app-name
- heroku buildpacks:set https://github.com/AdmitHub/meteor-buildpack-horse.git#devel --app your-app-name

## Push your App
- git add . 
- git commit -m "Deploy"
- heroku git:remote -a your-app-name
- git push heroku master

## Some Helpers
- heroku logs --tail --app your-app-name
- heroku ps:kill web.1 -a your-app-name
- heroku ps:restart web.1 -a your-app-name