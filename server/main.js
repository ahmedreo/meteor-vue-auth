import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base'

Meteor.startup(() => {
	Meteor.methods({
		updateUsername: function(username){
			var user = Accounts.setUsername(Meteor.userId() , username)
			return user;
		}
	});
});
