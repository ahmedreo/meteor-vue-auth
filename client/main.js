import {Meteor} from 'meteor/meteor';

// Styles
import '/imports/ui/sass/main.scss'

// Start Your App
import Vue from 'vue'
import App from '/imports/ui/App.vue'
import router from '/imports/ui/router'
import store from '/imports/ui/store'
import VueMeteorTracker from 'vue-meteor-tracker'

Vue.use(VueMeteorTracker)

Meteor.startup(() => {
	new Vue({
		el: '#app',
		router,
		store,
		render: (createElement) => {
			return createElement(App);
		}
	})
})
